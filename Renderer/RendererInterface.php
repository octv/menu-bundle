<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Renderer;

use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Interface for menu renderers
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface RendererInterface
{
	/**
	 * Render the given menu
	 * 
	 * @param  MenuInterface $menu
	 * 
	 * @return mixed
	 */
	public function renderMenu(MenuInterface $menu);
}