<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Renderer;

use Octave\Bundle\MenuBundle\Model\MenuInterface;
use Twig\Environment;

/**
 * Octave menu renderer for the Twig environment
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class TwigRenderer implements RendererInterface
{
	/**
	 * @var string
	 */
	protected $defaultTemplate;

	/**
	 * @var Environment
	 */
	protected $environment;

	/**
	 * TwigRenderer constructor
	 * 
	 * @param Environment $environment
	 */
	public function __construct(Environment $environment)
	{
		$this->environment = $environment;
	}

	/**
	 * Sets the default Twig rendering template for when no template is given
	 * This initially gets called from the configuration using the 'octave_menu.twig.default_template' option
	 * 
	 * @param string $template
	 */
	public function setDefaultTemplate(string $template)
	{
		$this->defaultTemplate = $template;
	}

	/**
	 * Renders the given menu with the given template
	 * 
	 * @param  MenuInterface $menu
	 * @param  string 		 $template
	 * 
	 * @return string
	 */
	public function renderMenuWithTemplate(MenuInterface $menu, string $template)
	{
		return $this->environment->render($template, [
			'menu' => $menu
		]);
	}

	/**
	 * Renders the given menu with the default template
	 * 
	 * {@inheritdoc}
	 */
	public function renderMenu(MenuInterface $menu)
	{
		return $this->renderMenuWithTemplate($menu, $this->defaultTemplate);
	}
}