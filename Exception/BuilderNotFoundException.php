<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * BuilderNotFoundException is thrown if a requested menu builder does not exist
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class BuilderNotFoundException extends Exception
{
	/**
	 * @var string
	 */
	private $builderName;

	/**
	 * @param string $builderName
	 */
	public function __construct(string $builderName)
	{
		$this->builderName = $builderName;
		parent::__construct(sprintf('There is no menu builder found for the name "%s"', $builderName), 1101);
	}
}
