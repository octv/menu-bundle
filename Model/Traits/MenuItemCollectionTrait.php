<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model\Traits;

use Octave\Bundle\MenuBundle\Model\MenuItem;
use Octave\Bundle\MenuBundle\Model\MenuItemCollectionInterface;
use Octave\Bundle\MenuBundle\Model\MenuItemInterface;

/**
 * Trait for a collection holding MenuItemInterface instances
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
trait MenuItemCollectionTrait
{
	/**
	 * @var array
	 */
	protected $children = [];

	/**
	 * {@inheritdoc}
	 */
	public function addChild(string $name, array $options = []): MenuItemInterface
	{
		$item = new MenuItem();

		$item
			->setName($name)
			->setParent($this)
			->setTranslationDomain(isset($options['translation_domain']) ? $options['translation_domain'] : null)
			->setRoute(isset($options['route']) ? $options['route'] : null)
			->setRouteParameters(isset($options['route_parameters']) ? $options['route_parameters'] : [])
			->setUri(isset($options['uri']) ? $options['uri'] : null)
			->setAttributes(isset($options['attributes']) ? $options['attributes'] : [])
		;

		if (isset($options['label'])) {
			$item->setLabel($options['label']);
		}
		if (isset($options['is_active'])) {
			$item->setIsActive($options['is_active']);
		}

		$this->children[$name] = $item;

		return $item;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasChild(string $name): bool
	{
		return isset($this->children[$name]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getChild(string $name): MenuItemInterface
	{
		return $this->children[$name];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getChildren(): array
	{
		return $this->children;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasChildren(): bool
	{
		return count($this->children) > 0;
	}

	/**
	 * {@inheritdoc}
	 */
	public function removeChild(string $name): MenuItemCollectionInterface
	{
		unset($this->children[$name]);

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function count(): int
	{
		return count($this->children);
	}
}