<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model;

/**
 * Interface for a menu instance
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface MenuInterface extends MenuItemCollectionInterface
{
	/**
	 * Returns the identifying name of the menu
	 * 
	 * @return string
	 */
	public function getName(): string;

	/**
	 * Sets the identifying name of the menu
	 * 
	 * @param  string $name
	 *
	 * @return MenuInterface
	 */
	public function setName(string $name): MenuInterface;

	/**
	 * Returns all attributes
	 * 
	 * @return array
	 */
	public function getAttributes(): array;

	/**
	 * Returns the attribute for the given name
	 *
	 * @param  string $attributeName
	 * 
	 * @return mixed
	 */
	public function getAttribute(string $attributeName);

	/**
	 * Replaces all attributes
	 * 
	 * @param  array $attributes
	 *
	 * @return MenuInterface
	 */
	public function setAttributes(array $attributes): MenuInterface;

	/**
	 * Sets the attribute for the given name and value combination
	 * 
	 * @param  string 	$attributeName
	 * @param  mixed 	$attributeValue
	 *
	 * @return MenuInterface
	 */
	public function addAttribute(string $attributeName, $attributeValue): MenuInterface;

	/**
	 * Checks if the menu has an attribute for the given name
	 * 
	 * @param  string $attributeName
	 *
	 * @return bool
	 */
	public function hasAttribute(string $attributeName): bool;

	/**
	 * Removes the attribute for the given name
	 * 
	 * @param  string $attributeName
	 *
	 * @return MenuInterface
	 */
	public function removeAttribute(string $attributeName): MenuInterface;
}