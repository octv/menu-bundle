<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model;

use Octave\Bundle\MenuBundle\Model\Traits\MenuItemCollectionTrait;

/**
 * Basic menu instance
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class Menu implements MenuInterface
{
	use MenuItemCollectionTrait;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * {@inheritdoc}
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setName(string $name): MenuInterface
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAttributes(): array
	{
		return $this->attributes;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAttribute(string $attributeName)
	{
		return $this->attributes[$attributeName];
	}

	/**
	 * {@inheritdoc}
	 */
	public function setAttributes(array $attributes): MenuInterface
	{
		$this->attributes = $attributes;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addAttribute(string $attributeName, $attributeValue): MenuInterface
	{
		$this->attributes[$attributeName] = $attributeValue;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasAttribute(string $attributeName): bool
	{
		return isset($this->attributes[$attributeName]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function removeAttribute(string $attributeName): MenuInterface
	{
		unset($this->attributes[$attributeName]);

		return $this;
	}
}