<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model;

/**
 * Interface for a menu item instance
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface MenuItemInterface extends MenuItemCollectionInterface
{
	/**
	 * Returns the identifying name of the menu
	 * 
	 * @return string
	 */
	public function getName(): string;

	/**
	 * Sets the identifying name of the menu
	 * 
	 * @param  string $name
	 *
	 * @return MenuItemInterface
	 */
	public function setName(string $name): MenuItemInterface;

	/**
	 * Returns the parent menu or item instance
	 * 
	 * @return MenuItemCollectionInterface
	 */
	public function getParent(): MenuItemCollectionInterface;

	/**
	 * Sets the parent menu or item instance
	 *
	 * @param  MenuItemCollectionInterface $parent
	 * 
	 * @return MenuItemInterface
	 */
	public function setParent(MenuItemCollectionInterface $parent): MenuItemInterface;

	/**
	 * Checks if this item is a root menu item
	 * 
	 * @return bool
	 */
	public function isRoot(): bool;

	/**
	 * Returns the label of the menu item
	 * If the translation domain is set, the label is used as a message key
	 * 
	 * @return string
	 */
	public function getLabel(): string;

	/**
	 * Sets the label of the menu
	 * If the translation domain is set, the label is used as a message key
	 * 
	 * @param  string $label
	 *
	 * @return MenuItemInterface
	 */
	public function setLabel(string $label): MenuItemInterface;

	/**
	 * Returns the translation domain of the menu item
	 * 
	 * @return ?string
	 */
	public function getTranslationDomain(): ?string;

	/**
	 * Sets the translation domain of the menu item
	 * If it's set to null, no translation domain will be used
	 *
	 * @param  ?string $translationDomain
	 * 
	 * @return MenuItemInterface
	 */
	public function setTranslationDomain(?string $translationDomain): MenuItemInterface;

	/**
	 * Returns the menu item route if it's set
	 * 
	 * @return ?string
	 */
	public function getRoute(): ?string;

	/**
	 * Sets the menu item route
	 * If it's set to null, and the uri is not set either, the menu item will be used as a placeholder
	 * 
	 * @param  string $route
	 *
	 * @return MenuItemInterface
	 */
	public function setRoute(?string $route): MenuItemInterface;

	/**
	 * Returns all route parameters
	 * 
	 * @return array
	 */
	public function getRouteParameters(): array;

	/**
	 * Returns the route parameter for the given name
	 *
	 * @param  string $routeParameterName
	 * 
	 * @return mixed
	 */
	public function getRouteParameter(string $routeParameterName);

	/**
	 * Replaces all route parameters
	 * 
	 * @param  array $routeParameters
	 *
	 * @return MenuItemInterface
	 */
	public function setRouteParameters(array $routeParameters): MenuItemInterface;

	/**
	 * Sets the route parameter for the given name and value combination
	 * 
	 * @param  string 	$routeParameterName
	 * @param  mixed 	$routeParameterValue
	 *
	 * @return MenuItemInterface
	 */
	public function addRouteParameter(string $routeParameterName, $routeParameterValue): MenuItemInterface;

	/**
	 * Checks if the menu item has an route parameter for the given name
	 * 
	 * @param  string $routeParameterName
	 *
	 * @return bool
	 */
	public function hasRouteParameter(string $routeParameterName): bool;

	/**
	 * Removes the route parameter for the given name
	 * 
	 * @param  string $routeParameterName
	 *
	 * @return MenuItemInterface
	 */
	public function removeRouteParameter(string $routeParameterName): MenuItemInterface;

	/**
	 * Returns the menu item uri
	 * 
	 * @return ?string
	 */
	public function getUri(): ?string;

	/**
	 * Sets the menu item uri
	 * If it's set to null, and the route is not set either, the menu item will be used as a placeholder
	 * 
	 * @param  string $uri
	 *
	 * @return MenuItemInterface
	 */
	public function setUri(?string $uri): MenuItemInterface;

	/**
	 * Returns all attributes
	 * 
	 * @return array
	 */
	public function getAttributes(): array;

	/**
	 * Returns the attribute for the given name
	 *
	 * @param  string $attributeName
	 * 
	 * @return mixed
	 */
	public function getAttribute(string $attributeName);

	/**
	 * Replaces all attributes
	 * 
	 * @param  array $attributes
	 *
	 * @return MenuItemInterface
	 */
	public function setAttributes(array $attributes): MenuItemInterface;

	/**
	 * Sets the attribute for the given name and value combination
	 * 
	 * @param  string 	$attributeName
	 * @param  mixed 	$attributeValue
	 *
	 * @return MenuItemInterface
	 */
	public function addAttribute(string $attributeName, $attributeValue): MenuItemInterface;

	/**
	 * Checks if the menu item has an attribute for the given name
	 * 
	 * @param  string $attributeName
	 *
	 * @return bool
	 */
	public function hasAttribute(string $attributeName): bool;

	/**
	 * Removes the attribute for the given name
	 * 
	 * @param  string $attributeName
	 *
	 * @return MenuItemInterface
	 */
	public function removeAttribute(string $attributeName): MenuItemInterface;

	/**
	 * Returns if the menu item is currently active
	 * If null is given, the active status of the menu item is not yet initialized
	 * 
	 * @return bool
	 */
	public function isActive(): ?bool;

	/**
	 * Sets the active status of the menu item
	 * 
	 * @param  bool $isActive
	 *
	 * @return MenuItemInterface
	 */
	public function setIsActive(bool $isActive): MenuItemInterface;

	/**
	 * Returns the nested level of the menu item
	 * The level starts at zero, meaning it's a root element of a menu
	 * 
	 * @return int
	 */
	public function getNestedLevel(): int;
}