<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model;

use Octave\Bundle\MenuBundle\Model\MenuItemInterface;

/**
 * Interface for a collection holding MenuItemInterface instances
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface MenuItemCollectionInterface extends \Countable
{
	/**
	 * Adds a menu item to the collection
	 * 
	 * @param  string 	$name
	 * @param  array 	$options
	 *
	 * @return MenuItemInterface
	 */
	public function addChild(string $name, array $options = []): MenuItemInterface;

	/**
	 * Checks if there is a menu item in the collection for the given name
	 * 
	 * @param  string $name
	 *
	 * @return bool
	 */
	public function hasChild(string $name): bool;

	/**
	 * Returns the menu item in the collection for the given name
	 * You should always use the hasChild function first to make sure the item is in the collection
	 * 
	 * @param  string $name
	 *
	 * @return MenuItemInterface
	 */
	public function getChild(string $name): MenuItemInterface;

	/**
	 * Removes the menu item from the collection for the given name
	 * 
	 * @param  string $name
	 *
	 * @return MenuItemCollectionInterface
	 */
	public function removeChild(string $name): MenuItemCollectionInterface;

	/**
	 * Returns all menu items in the collection
	 * 
	 * @return array
	 */
	public function getChildren(): array;

	/**
	 * Checks if the collection has children
	 * 
	 * @return bool
	 */
	public function hasChildren(): bool;
}