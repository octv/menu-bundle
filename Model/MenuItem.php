<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Model;

use Octave\Bundle\MenuBundle\Model\Traits\MenuItemCollectionTrait;

/**
 * Basic menu item instance
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuItem implements MenuItemInterface
{
	use MenuItemCollectionTrait;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var MenuItemCollectionInterface
	 */
	protected $parent;

	/**
	 * @var ?string
	 */
	protected $label = null;

	/**
	 * @var ?string
	 */
	protected $translationDomain = null;

	/**
	 * @var ?string
	 */
	protected $route = null;

	/**
	 * @var array
	 */
	protected $routeParameters = [];

	/**
	 * @var ?string
	 */
	protected $uri = null;

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * @var ?bool
	 */
	protected $isActive = null;

	/**
	 * {@inheritdoc}
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setName(string $name): MenuItemInterface
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParent(): MenuItemCollectionInterface
	{
		return $this->parent;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setParent(MenuItemCollectionInterface $parent): MenuItemInterface
	{
		$this->parent = $parent;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isRoot(): bool
	{
		return $this->parent instanceof MenuInterface;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getLabel(): string
	{
		return $this->label ? $this->label : $this->name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setLabel(string $label): MenuItemInterface
	{
		$this->label = $label;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTranslationDomain(): ?string
	{
		return $this->translationDomain;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setTranslationDomain(?string $translationDomain): MenuItemInterface
	{
		$this->translationDomain = $translationDomain;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRoute(): ?string
	{
		return $this->route;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRoute(?string $route): MenuItemInterface
	{
		$this->route = $route;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRouteParameters(): array
	{
		return $this->routeParameters;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRouteParameter(string $routeParameterName)
	{
		return $this->routeParameters[$routeParameterName];
	}

	/**
	 * {@inheritdoc}
	 */
	public function setRouteParameters(array $routeParameters): MenuItemInterface
	{
		$this->routeParameters = $routeParameters;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addRouteParameter(string $routeParameterName, $routeParameterValue): MenuItemInterface
	{
		$this->routeParameters[$routeParameterName] = $routeParameterValue;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasRouteParameter(string $routeParameterName): bool
	{
		return isset($this->routeParameters[$routeParameterName]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function removeRouteParameter(string $routeParameterName): MenuItemInterface
	{
		unset($this->routeParameters[$routeParameterName]);

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getUri(): ?string
	{
		return $this->uri;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setUri(?string $uri): MenuItemInterface
	{
		$this->uri = $uri;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAttributes(): array
	{
		return $this->attributes;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAttribute(string $attributeName)
	{
		return $this->attributes[$attributeName];
	}
	/**
	 * {@inheritdoc}
	 */
	public function setAttributes(array $attributes): MenuItemInterface
	{
		$this->attributes = $attributes;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addAttribute(string $attributeName, $attributeValue): MenuItemInterface
	{
		$this->attributes[$attributeName] = $attributeValue;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasAttribute(string $attributeName): bool
	{
		return isset($this->attributes[$attributeName]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function removeAttribute(string $attributeName): MenuItemInterface
	{
		unset($this->attributes[$attributeName]);

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isActive(): ?bool
	{
		return $this->isActive;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setIsActive(bool $isActive): MenuItemInterface
	{
		$this->isActive = $isActive;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNestedLevel(): int
	{
		if ($this->parent instanceof MenuItemInterface) {
			return $this->parent->getNestedLevel() + 1;
		}

		return 0;
	}
}