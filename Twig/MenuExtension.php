<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Twig;

use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Octave\Bundle\MenuBundle\Renderer\TwigRenderer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Octave menu Twig extension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuExtension extends AbstractExtension
{
	/**
	 * @var MenuFactory
	 */
	private $factory;

	/**
	 * @var TwigRenderer
	 */
	private $renderer;

	/**
	 * MenuExtension constructor
	 *
	 * @param MenuFactory $factory
	 * @param TwigRenderer $renderer
	 */
	public function __construct(MenuFactory $factory, TwigRenderer $renderer)
	{
		$this->factory = $factory;
		$this->renderer = $renderer;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFunctions()
	{
		return [
			new TwigFunction('octave_menu', [$this, 'renderMenu'], ['is_safe' => ['html']])
		];
	}

	/**
	 * Render a menu for the given name and given template with the given attributes
	 * If no template is given, it will fallback to the default template
	 * 
	 * @param  string 	$menuName
	 * @param  array 	$attributes
	 * @param  ?string 	$template
	 * 
	 * @return string 	The rendered template HTML
	 */
	public function renderMenu(string $menuName, array $attributes = [], ?string $template = null)
	{
		$menu = $this->factory->create($menuName, $attributes);

		if ($template) {
			return $this->renderer->renderMenuWithTemplate($menu, $template);
		} else {
			return $this->renderer->renderMenu($menu);
		}		
	}
}