<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Octave menu configuration
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('octave_menu');

		$rootNode
			->children()
				->arrayNode('twig')
					->addDefaultsIfNotSet()
					->children()
						->scalarNode('default_template')->defaultValue('@OctaveMenu/menu.html.twig')->end()
					->end()
				->end()
			->end()
		;

		return $treeBuilder;
	}
}