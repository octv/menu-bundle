<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Adds menu extensions to the according menu builder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class ExtensionPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		if (!$container->has(MenuFactory::class)) {
			return;
		}

		$factory = $container->findDefinition(MenuFactory::class);

		// Sort the extensions based on priority and get their builder name
		$extensions = $container->findTaggedServiceIds('octave.menu.extension');
		$sortedExtensions = [];

		foreach ($extensions as $id => $tags) {
			$priority = isset($tags[0]['priority']) ? $tags[0]['priority'] : 0;
			$sortedExtensions[$priority][] = [ new Reference($id), $tags[0]['builder'] ];
		}

		if ($sortedExtensions) {
			krsort($sortedExtensions);
			$sortedExtensions = array_merge(...$sortedExtensions);
		}

		foreach ($sortedExtensions as $extension) {
			$factory->addMethodCall('addExtension', $extension);
		}
	}
}