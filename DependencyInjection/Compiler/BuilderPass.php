<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Adds all builder services to the MenuFactory
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class BuilderPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		if (!$container->has(MenuFactory::class)) {
			return;
		}

		$factory = $container->findDefinition(MenuFactory::class);

		$builders = $container->findTaggedServiceIds('octave.menu.builder');

		foreach ($builders as $id => $tags) {
			$key = isset($tags[0]['key']) ? $tags[0]['key'] : $id;
			$factory->addMethodCall('addBuilder', [new Reference($id), $key]);
		}
	}
}