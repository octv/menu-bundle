<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Adds all matcher voter services to the ItemMatcher service
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class VoterPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		if (!$container->has(ItemMatcher::class)) {
			return;
		}

		$matcher = $container->findDefinition(ItemMatcher::class);

		$voters = $container->findTaggedServiceIds('octave.menu.matcher_voter');

		foreach ($voters as $id => $tags) {
			$matcher->addMethodCall('addVoter', [new Reference($id)]);
		}
	}
}