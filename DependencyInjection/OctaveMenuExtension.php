<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\DependencyInjection;

use Octave\Bundle\MenuBundle\Renderer\TwigRenderer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Octave menu extension for bundle configuration loading
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveMenuExtension extends Extension
{
	public function load(array $configs, ContainerBuilder $container)
	{
		// Setup the YAML loader for the config Resources/config folder
		$loader = new YamlFileLoader(
			$container,
			new FileLocator(__DIR__.'/../Resources/config')
		);

		// Load the services
		$loader->load('services.yaml');

		$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);

		// Set the Twig default template
		$twigRenderer = $container->getDefinition(TwigRenderer::class);
		$twigRenderer->addMethodCall('setDefaultTemplate', [$config['twig']['default_template']]);
	}
}