<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Factory;

use Octave\Bundle\MenuBundle\Builder\BuilderInterface;
use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Exception\BuilderNotFoundException;
use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Factory for creating menu's
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuFactory
{
	/**
	 * @var BuilderInterface[]
	 */
	protected $builders = [];

	/**
	 * @var ItemMatcher
	 */
	protected $matcher;

	/**
	 * MenuFactory constructor
	 *
	 * @param ItemMatcher $matcher
	 */
	public function __construct(ItemMatcher $matcher)
	{
		$this->matcher = $matcher;
	}

	/**
	 * Adds a builder to the collection
	 *
	 * @param BuilderInterface 	$builder
	 * @param string 			$name
	 */
	public function addBuilder(BuilderInterface $builder, string $name)
	{
		$this->builders[$name] = $builder;
	}

	/**
	 * Adds an extension to a builder
	 *
	 * @param ExtensionInterface 	$extension
	 * @param string 				$builderName
	 *
	 * @throws BuilderNotFoundException
	 */
	public function addExtension(ExtensionInterface $extension, string $builderName)
	{
		if (!isset($this->builders[$builderName])) {
			throw new BuilderNotFoundException($builderName);
		}

		$builder = $this->builders[$builderName];
		$builder->addExtension($extension);
	}

	/**
	 * Creates a menu for the given name and attributes
	 *
	 * @param  string $name
	 * @param  array $attributes
	 *
	 * @throws BuilderNotFoundException
	 *
	 * @return MenuInterface
	 */
	public function create(string $name, array $attributes = []): MenuInterface
	{
		if (!isset($this->builders[$name])) {
			throw new BuilderNotFoundException($name);
		}

		$builder = $this->builders[$name];
		$menu = $builder->build($attributes);

		$this->matcher->matchCollection($menu);

		return $menu;
	}
}