Octave Menu Bundle
=======

The Octave Menu Bundle is used to generate menus for Symfony 4. It does not have dependencies on other Octave bundles.

[![Pipeline Status](https://gitlab.com/octv/menu-bundle/badges/master/pipeline.svg)](https://gitlab.com/octv/menu-bundle/commits/master)
[![Code Coverage](https://gitlab.com/octv/menu-bundle/badges/master/coverage.svg)](https://gitlab.com/octv/menu-bundle/commits/master)
[![PHPStan Level](https://img.shields.io/badge/PHPStan-level%207-brightgreen.svg?style=flat)](https://gitlab.com/octv/menu-bundle/commits/master)
[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/octv/menu-bundle/blob/master/LICENSE)

## Why?

Why should you use this bundle? It's created mainly for learning purposes, but also aimed at building extendable menus.
You can create a menu with your core menu items in bundle A and extend this menu using extensions in bundle B and C without too much of a hassle.

## Installation

Install the bundle with composer, Symfony Flex will set it up for you!

```bash
$ composer require octv/menu-bundle
```

>  If you do not have Symfony Flex installed, add the bundle to your bundles.php manually.

## Getting started

For every menu you use you will need to create a menu builder service:

```php
namespace App\Menu;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

class SidebarMenuBuilder extends AbstractBuilder
{
	protected function initialize(MenuInterface $menu, array $attributes)
	{
		// Add your base menu items here
	}
}
```

Make sure to tag your service with `octave.menu.builder`, and to pass your menu name as the `key`.

```yaml
services: 
    App\Menu\MainMenuBuilder:
        tags: 
            - { name: 'octave.menu.builder', key: 'app.main_menu' }
```

## Extending your menu

You can create an extension to add items to the menu outside of your builder service.
To create an extension, create a service implementing ExtensionInterface:

```php
namespace App\Menu;

use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

class SidebarMenuExtension implements ExtensionInterface
{
	public function extend(MenuInterface $menu)
	{
		
	}
}
```

Tag your service with `octave.menu.extension`, and pass your menu builder's key/name to the `builder` option.
Optionally you can also pass the priority of the extension to determine the order of all extensions for the same builder.

```yaml
services:
    App\Menu\MainMenuExtension:
        tags: 
            - { name: 'octave.menu.extension', builder: 'app.main_menu', priority: 0 }
```

## Twig

To render a menu in your Twig template, you can use the `octave_menu` function.
It takes the name of the menu as the first parameter, optional attributes array as the second, and the optional template as the third. If no template is given, it will fallback to the default template.

Example:

```twig
{{ octave_menu('app.main_menu') }}
{{ octave_menu('app.main_menu', { attributeName: 'attributeValue' }) }}
{{ octave_menu('app.main_menu', { attributeName: 'attributeValue' }, 'menu/my_custom_template.html.twig') }}
```

## Configuration

The following options can be configured in `config/packages/octave_menu.yaml`.

```yaml
octave_menu:
    twig:
        default_template: '@OctaveMenu/menu.html.twig' # This template will be used when no template is given to the Twig renderer
```

## Credits

This bundle has been heavily inspired by [KnpMenu](https://github.com/KnpLabs/KnpMenu).
Also a big shoutout to the Symfony Slack community for some code reviews!