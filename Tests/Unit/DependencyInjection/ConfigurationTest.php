<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\DependencyInjection;

use Octave\Bundle\MenuBundle\DependencyInjection\Configuration;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Config\Definition\Processor;

/**
 * Unit tests for Octave\Bundle\MenuBundle\DependencyInjection\Configuration
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class ConfigurationTest extends WebTestCase
{
	/**
	 * Test if the Twig configuration gets loaded properly
	 */
	public function testTwig()
	{
		$processor = new Processor();

		// Test without input
		$config = $processor->processConfiguration(new Configuration(), []);
		$this->assertEquals('@OctaveMenu/menu.html.twig', $config['twig']['default_template']);

		// Test with input
		$twigInput = [
			'default_template' => 'testTemplate.html.twig'
		];

		$config = $processor->processConfiguration(new Configuration(), [
			[ 'twig' => $twigInput ]
		]);
		$this->assertEquals($twigInput, $config['twig']);
	}
}