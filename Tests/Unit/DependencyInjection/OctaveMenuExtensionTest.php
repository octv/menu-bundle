<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\DependencyInjection;

use Octave\Bundle\MenuBundle\DependencyInjection\OctaveMenuExtension;
use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit tests for Octave\Bundle\MenuBundle\DependencyInjection\OctaveMenuExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveMenuExtensionTest extends WebTestCase
{
	/**
	 * Test if the extension loads the configuration properly
	 */
	public function testLoad()
	{
		$extension = new OctaveMenuExtension();
		$container = new ContainerBuilder();
		$extension->load([], $container);

		// Test one of the services
		$this->assertTrue($container->hasDefinition(MenuFactory::class));
	}
}