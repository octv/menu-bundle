<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Factory;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Exception\BuilderNotFoundException;
use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Model\MenuInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Factory\MenuFactory
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuFactoryTest extends WebTestCase
{
	/**
	 * Test if adding a builder to the factory works
	 */
	public function testAddBuilder()
	{
		$factory = $this->getMenuFactory();
		$builder = $this->getMockForAbstractClass(AbstractBuilder::class);

		$factory->addBuilder($builder, 'testBuilder');
		$menu = $factory->create('testBuilder');

		$this->assertTrue($menu instanceof Menu);
	}

	/**
	 * Test if creating a menu with a non-existing menu name throws a BuilderNotFoundException
	 */
	public function testCreateNonExistingMenu()
	{
		$factory = $this->getMenuFactory();

		$this->expectException(BuilderNotFoundException::class);

		$factory->create('nonExistingBuilder');
	}

	/**
	 * Test if adding an extension to the factory with an incorrect builder name throws a BuilderNotFoundException
	 */
	public function testAddExtensionToNonExistingMenu()
	{
		$factory = $this->getMenuFactory();
		$extension = $this->createMock(ExtensionInterface::class);

		$this->expectException(BuilderNotFoundException::class);

		$factory->addExtension($extension, 'nonExistingBuilder');
	}

	/**
	 * @return MenuFactory     
	 */
	protected function getMenuFactory(): MenuFactory
	{
		$itemMatcher = $this->createMock(ItemMatcher::class);
		$menuFactory = new MenuFactory($itemMatcher);

		return $menuFactory;
	}
}