<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Model;

use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Model\MenuItem;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Model\MenuItem
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuItemTest extends WebTestCase
{
	/**
	 * Test name
	 */
	public function testName()
	{
		$menuItem = new MenuItem();
		$menuItem->setName('Test name');
		$this->assertEquals($menuItem->getName(), 'Test name');
	}

	/**
	 * Test parent
	 */
	public function testParent()
	{
		$menuItem = new MenuItem();
		$parentMenuItem = new MenuItem();
		$menuItem->setParent($parentMenuItem);
		$this->assertEquals($menuItem->getParent(), $parentMenuItem);
	}

	/**
	 * Test is root
	 */
	public function testIsRoot()
	{
		$menuItem = new MenuItem();
		$parentMenuItem = new MenuItem();
		$menuItem->setParent($parentMenuItem);
		$this->assertFalse($menuItem->isRoot());

		$menu = new Menu();
		$menuItem->setParent($menu);
		$this->assertTrue($menuItem->isRoot());
	}

	/**
	 * Test label
	 */
	public function testLabel()
	{
		$menuItem = new MenuItem();
		$menuItem->setName('Test name');
		$this->assertEquals($menuItem->getLabel(), 'Test name');

		$menuItem->setName('Test label');
		$this->assertEquals($menuItem->getLabel(), 'Test label');
	}

	/**
	 * Test translation domain
	 */
	public function testTranslationDomain()
	{
		$menuItem = new MenuItem();
		$this->assertEquals($menuItem->getTranslationDomain(), null);
		$menuItem->setTranslationDomain('test_translation_domain');
		$this->assertEquals($menuItem->getTranslationDomain(), 'test_translation_domain');
	}

	/**
	 * Test route
	 */
	public function testRoute()
	{
		$menuItem = new MenuItem();
		$this->assertEquals($menuItem->getRoute(), null);
		$menuItem->setRoute('test_route');
		$this->assertEquals($menuItem->getRoute(), 'test_route');
	}

	/**
	 * Test route parameters
	 */
	public function testRouteParameters()
	{
		$menuItem = new MenuItem();
		
		$this->assertEquals(count($menuItem->getRouteParameters()), 0);
		$this->assertFalse($menuItem->hasRouteParameter('testRouteParameter'));

		$menuItem->setRouteParameters([
			'testRouteParameter' => 'testValue'
		]);

		$this->assertEquals(count($menuItem->getRouteParameters()), 1);
		$this->assertTrue($menuItem->hasRouteParameter('testRouteParameter'));
		$this->assertEquals($menuItem->getRouteParameter('testRouteParameter'), 'testValue');

		$menuItem->removeRouteParameter('testRouteParameter');

		$this->assertFalse($menuItem->hasRouteParameter('testRouteParameter'));

		$menuItem->addRouteParameter('testRouteParameter', 'testValue');

		$this->assertTrue($menuItem->hasRouteParameter('testRouteParameter'));
		$this->assertEquals($menuItem->getRouteParameter('testRouteParameter'), 'testValue');
	}

	/**
	 * Test uri
	 */
	public function testUri()
	{
		$menuItem = new MenuItem();
		$this->assertEquals($menuItem->getUri(), null);
		$menuItem->setUri('test_uri');
		$this->assertEquals($menuItem->getUri(), 'test_uri');
	}

	/**
	 * Test attributes
	 */
	public function testAttributes()
	{
		$menuItem = new MenuItem();
		
		$this->assertEquals(count($menuItem->getAttributes()), 0);
		$this->assertFalse($menuItem->hasAttribute('testAttribute'));

		$menuItem->setAttributes([
			'testAttribute' => 'testValue'
		]);

		$this->assertEquals(count($menuItem->getAttributes()), 1);
		$this->assertTrue($menuItem->hasAttribute('testAttribute'));
		$this->assertEquals($menuItem->getAttribute('testAttribute'), 'testValue');

		$menuItem->removeAttribute('testAttribute');

		$this->assertFalse($menuItem->hasAttribute('testAttribute'));

		$menuItem->addAttribute('testAttribute', 'testValue');

		$this->assertTrue($menuItem->hasAttribute('testAttribute'));
		$this->assertEquals($menuItem->getAttribute('testAttribute'), 'testValue');
	}

	/**
	 * Test is active
	 */
	public function testIsActive()
	{
		$menuItem = new MenuItem();
		$this->assertEquals($menuItem->isActive(), null);
		$menuItem->setIsActive(false);
		$this->assertFalse($menuItem->isActive());
		$menuItem->setIsActive(true);
		$this->assertTrue($menuItem->isActive());
	}

	/**
	 * Test nested level
	 */
	public function testNestedLevel()
	{
		$menuItem = new MenuItem();

		$this->assertEquals($menuItem->getNestedLevel(), 0);

		$menu = new Menu();
		$menuItem->setParent($menu);

		$this->assertEquals($menuItem->getNestedLevel(), 0);

		$parentMenuItem = new MenuItem();
		$parentMenuItem->setParent($menu);
		$menuItem->setParent($parentMenuItem);

		$this->assertEquals($menuItem->getNestedLevel(), 1);
	}
}