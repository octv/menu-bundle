<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Model;

use Octave\Bundle\MenuBundle\Model\Menu;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Model\Menu
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuTest extends WebTestCase
{
	/**
	 * Test name
	 */
	public function testName()
	{
		$menu = new Menu();
		$menu->setName('Test name');
		$this->assertEquals($menu->getName(), 'Test name');
	}

	/**
	 * Test attributes
	 */
	public function testAttributes()
	{
		$menu = new Menu();
		
		$this->assertEquals(count($menu->getAttributes()), 0);
		$this->assertFalse($menu->hasAttribute('testAttribute'));

		$menu->setAttributes([
			'testAttribute' => 'testValue'
		]);

		$this->assertEquals(count($menu->getAttributes()), 1);
		$this->assertTrue($menu->hasAttribute('testAttribute'));
		$this->assertEquals($menu->getAttribute('testAttribute'), 'testValue');

		$menu->removeAttribute('testAttribute');

		$this->assertFalse($menu->hasAttribute('testAttribute'));

		$menu->addAttribute('testAttribute', 'testValue');

		$this->assertTrue($menu->hasAttribute('testAttribute'));
		$this->assertEquals($menu->getAttribute('testAttribute'), 'testValue');
	}
}