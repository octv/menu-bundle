<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Model\Traits;

use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Model\Traits\MenuItemCollectionTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Model\Traits\MenuItemCollectionTrait
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuItemCollectionTraitTest extends WebTestCase
{
	/**
	 * Test all children functions
	 */
	public function testChildren()
	{
		// Test it with an actual instance that uses the trait and does not override anything, so $this variables work
		$collection = new Menu();
		$this->assertTrue(isset(class_uses($collection)[MenuItemCollectionTrait::class]));

		$this->assertEquals($collection->count(), 0);
		$this->assertFalse($collection->hasChildren());
		$this->assertEquals($collection->getChildren(), []);
		$this->assertFalse($collection->hasChild('testChild'));

		// Add a child without options
		$collection->addChild('testChildWithoutOptions');
		$this->assertTrue($collection->hasChild('testChildWithoutOptions'));
		$this->assertEquals($collection->getChild('testChildWithoutOptions')->getParent(), $collection);
		$this->assertEquals($collection->count(), 1);
		$this->assertTrue($collection->hasChildren());

		$collection->removeChild('testChildWithoutOptions');
		$this->assertFalse($collection->hasChild('testChildWithoutOptions'));
		$this->assertEquals($collection->count(), 0);
		$this->assertFalse($collection->hasChildren());

		// Add a child with options
		$collection->addChild('testChild', [
			'translation_domain' => 'test_domain',
			'route' => 'test_route',
			'route_parameters' => [ 'test_parameter' => 'test_value' ],
			'uri' => 'test_uri',
			'attributes' => [ 'test_attribute' => 'test_value' ],
			'label' => 'Test label',
			'is_active' => true
		]);
		$this->assertTrue($collection->hasChild('testChild'));
	}
}