<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Twig;

use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Octave\Bundle\MenuBundle\Renderer\TwigRenderer;
use Octave\Bundle\MenuBundle\Twig\MenuExtension;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Twig\MenuExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuExtensionTest extends WebTestCase
{
	/**
	 * Test if all functions are initialized
	 */
	public function testFunctions()
	{
		$renderer = $this->createMock(TwigRenderer::class);;
		$factory = $this->createMock(MenuFactory::class);
		$extension = new MenuExtension($factory, $renderer);

		$this->assertEquals(count($extension->getFunctions()), 1);
		$this->assertEquals($extension->getFunctions()[0]->getName(), 'octave_menu');
	}
}