<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit\Builder;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Model\Menu;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\MenuBundle\Builder\AbstractBuilder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class AbstractBuilderTest extends WebTestCase
{
	/**
	 * Test if a menu can be built
	 */
	public function testBuild()
	{
		$builder = $this->getMockForAbstractClass(AbstractBuilder::class);
		$menu = $builder->build([]);

		$this->assertInstanceOf(Menu::class, $menu);
		$this->assertEquals($menu->count(), 0);
	}
}