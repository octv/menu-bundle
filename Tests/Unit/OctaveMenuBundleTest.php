<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Unit;

use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\BuilderPass;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\ExtensionPass;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\VoterPass;
use Octave\Bundle\MenuBundle\OctaveMenuBundle;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit tests for Octave\Bundle\MenuBundle\OctaveMenuBundle
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveMenuBundleTest extends WebTestCase
{
	/**
	 * Test if the bundle builds the compiler passes correctly
	 */
	public function testCompilerPasses()
	{
		$bundle = new OctaveMenuBundle();
		$container = new ContainerBuilder();
		$bundle->build($container);

		$passes = $container->getCompiler()->getPassConfig()->getBeforeOptimizationPasses();
		$passClassNames = [];

		foreach ($passes as $pass)
		{
			$passClassNames[] = get_class($pass);
		}

		$this->assertTrue(in_array(BuilderPass::class, $passClassNames));
		$this->assertTrue(in_array(ExtensionPass::class, $passClassNames));
		$this->assertTrue(in_array(VoterPass::class, $passClassNames));
	}
}