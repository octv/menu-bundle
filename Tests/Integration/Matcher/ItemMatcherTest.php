<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Matcher;

use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Octave\Bundle\MenuBundle\Matcher\Voter\VoterInterface;
use Octave\Bundle\MenuBundle\Model\Menu;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Matcher\ItemMatcher
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class ItemMatcherTest extends WebTestCase
{
	/**
	 * Test if matching a menu without a voter works
	 */
	public function testMatchWithoutVoter()
	{
		$matcher = new ItemMatcher();

		$menu = new Menu();
		$testItem1 = $menu->addChild('testItem1');
		$testSubItem = $testItem1->addChild('testSubItem');
		$testItem2 = $menu->addChild('testItem2');
		
		$matcher->matchCollection($menu);

		$this->assertFalse($testItem1->isActive());
		$this->assertFalse($testSubItem->isActive());
		$this->assertFalse($testItem2->isActive());
	}

	/**
	 * Test if matching a menu with a voter works
	 */
	public function testMatchWithVoter()
	{
		$matcher = new ItemMatcher();
		$voter = $this->createMock(VoterInterface::class);

		$matcher->addVoter($voter);

		$menu = new Menu();
		$testItem1 = $menu->addChild('testItem1');
		$testSubItem = $testItem1->addChild('testSubItem');
		$testItem2 = $menu->addChild('testItem2');

		// Vote the test sub item to true
		$voter
			->expects($this->at(1))
			->method('vote')
			->willReturn(true)
		;
		
		$matcher->matchCollection($menu);

		$this->assertTrue($testItem1->isActive());
		$this->assertTrue($testSubItem->isActive());
		$this->assertFalse($testItem2->isActive());
	}
}