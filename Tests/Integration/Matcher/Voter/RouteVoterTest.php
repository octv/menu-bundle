<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Matcher\Voter;

use Octave\Bundle\MenuBundle\Matcher\Voter\RouteVoter;
use Octave\Bundle\MenuBundle\Model\MenuItem;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Matcher\Voter\RouteVoter
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class RouteVoterTest extends WebTestCase
{
	/**
	 * Test voting without a route on the item
	 */
	public function testVoteWithoutRoute()
	{
		$requestStack = new RequestStack();
		$routeVoter = new RouteVoter($requestStack);

		$this->assertFalse($routeVoter->vote(new MenuItem()));
	}

	/**
	 * Test voting without a request in the request stack
	 */
	public function testVoteWithoutRequest()
	{
		$requestStack = new RequestStack();
		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');

		$this->assertFalse($routeVoter->vote($item));
	}

	/**
	 * Test voting with a different route
	 */
	public function testVoteOtherRoute()
	{
		$requestStack = new RequestStack();
		$request = new Request([], [], [
			'_route' => 'test_other_route'
		]);
		$requestStack->push($request);

		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');

		$this->assertFalse($routeVoter->vote($item));
	}

	/**
	 * Test voting with the same route
	 */
	public function testVoteRoute()
	{
		$requestStack = new RequestStack();
		$request = new Request([], [], [
			'_route' => 'test_route'
		]);
		$requestStack->push($request);

		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');

		$this->assertTrue($routeVoter->vote($item));
	}

	/**
	 * Test voting with the same route and other parameters
	 */
	public function testVoteRouteOtherParameters()
	{
		$requestStack = new RequestStack();
		$request = new Request([], [], [
			'_route' => 'test_route',
			'_route_params' => [
				'testAttributeName1' => 'testAttributeValue1',
				'testAttributeName2' => 'otherValue'
			]
		]);
		$requestStack->push($request);

		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');
		$item->setRouteParameters([
			'testAttributeName1' => 'testAttributeValue1',
			'testAttributeName2' => 'testAttributeValue2'
		]);

		$this->assertFalse($routeVoter->vote($item));
	}

	/**
	 * Test voting with the same route and missing parameters
	 */
	public function testVoteRouteMissingParameters()
	{
		$requestStack = new RequestStack();
		$request = new Request([], [], [
			'_route' => 'test_route',
			'_route_params' => [
				'testAttributeName1' => 'testAttributeValue1'
			]
		]);
		$requestStack->push($request);

		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');
		$item->setRouteParameters([
			'testAttributeName1' => 'testAttributeValue1',
			'testAttributeName2' => 'testAttributeValue2'
		]);

		$this->assertFalse($routeVoter->vote($item));
	}

	/**
	 * Test voting with the same route and parameters
	 */
	public function testVoteRouteWithParameters()
	{
		$requestStack = new RequestStack();
		$request = new Request([], [], [
			'_route' => 'test_route',
			'_route_params' => [
				'testAttributeName1' => 'testAttributeValue1',
				'testAttributeName2' => 'testAttributeValue2'
			]
		]);
		$requestStack->push($request);

		$routeVoter = new RouteVoter($requestStack);

		$item = new MenuItem();
		$item->setRoute('test_route');
		$item->setRouteParameters([
			'testAttributeName1' => 'testAttributeValue1',
			'testAttributeName2' => 'testAttributeValue2'
		]);

		$this->assertTrue($routeVoter->vote($item));
	}
}