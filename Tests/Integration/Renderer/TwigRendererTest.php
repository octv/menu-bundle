<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Renderer;

use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Renderer\TwigRenderer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Renderer\TwigRenderer
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class TwigRendererTest extends WebTestCase
{
	/**
	 * Test rendering without a template
	 */
	public function testRenderWithoutTemplate()
	{
		$renderer = $this->createRenderer();
		$renderer->setDefaultTemplate('menu.html.twig');
		$html = $renderer->renderMenu(new Menu());

		$this->assertEquals($html, 'TODO: Render menu');
	}

	/**
	 * Test rendering with a template
	 */
	public function testRenderWithTemplate()
	{
		$renderer = $this->createRenderer();
		$html = $renderer->renderMenuWithTemplate(new Menu(), 'menu.html.twig');

		$this->assertEquals($html, 'TODO: Render menu');
	}

	/**
	 * @return TwigRenderer
	 */
	private function createRenderer()
	{
		$loader = new FilesystemLoader(__DIR__.'/../../../Resources/views');
		$environment = new Environment($loader);
		$renderer = new TwigRenderer($environment);

		return $renderer;
	}
}