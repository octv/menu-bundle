<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Factory;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Octave\Bundle\MenuBundle\Model\MenuInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Factory\MenuFactory
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuFactoryTest extends WebTestCase
{
	/**
	 * Test if adding an extension to the factory works
	 */
	public function testAddExtension()
	{
		$factory = $this->getMenuFactory();
		$extension = $this->createMock(ExtensionInterface::class);
		$builder = $this->getMockForAbstractClass(AbstractBuilder::class);

		$factory->addBuilder($builder, 'testBuilder');
		$factory->addExtension($extension, 'testBuilder');

		$extension
			->expects($this->once())
			->method('extend')
			->with(
				$this->callback(function(MenuInterface $menu) {
					$menu->addChild('extensionChild');
					return true;
				})
			);
		;

		$menu = $factory->create('testBuilder');

		$this->assertTrue($menu->hasChild('extensionChild'));
	}

	/**
	 * @return MenuFactory     
	 */
	protected function getMenuFactory(): MenuFactory
	{
		$itemMatcher = $this->createMock(ItemMatcher::class);
		$menuFactory = new MenuFactory($itemMatcher);

		return $menuFactory;
	}
}