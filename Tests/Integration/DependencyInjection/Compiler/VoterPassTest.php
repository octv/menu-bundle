<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\VoterPass;
use Octave\Bundle\MenuBundle\DependencyInjection\OctaveMenuExtension;
use Octave\Bundle\MenuBundle\Matcher\ItemMatcher;
use Octave\Bundle\MenuBundle\Matcher\Voter\VoterInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Integration tests for Octave\Bundle\MenuBundle\DependencyInjection\Compiler\VoterPass
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class VoterPassTest extends WebTestCase
{
	/**
	 * Test if adding voters to the ItemMatcher service works
	 */
	public function testProcess()
	{
		$pass = new VoterPass();
		$container = new ContainerBuilder();

		// Load services
		$extension = new OctaveMenuExtension();
		$extension->load([], $container);

		$itemMatcherDefinition = $container->getDefinition(ItemMatcher::class);
		$pass->process($container);
		
		// Without a voter service the addVoter method in the item matcher should only have been called by the RouteVoter
		$this->assertTrue($itemMatcherDefinition->hasMethodCall('addVoter'));
		$this->assertEquals(count($itemMatcherDefinition->getMethodCalls()), 1);

		// Clear the method call for another test
		$itemMatcherDefinition->removeMethodCall('addVoter');

		// Mock a voter, the addVoter method should now have been called twice in the item matcher
		$voterDefinition = new Definition(VoterInterface::class);
		$voterDefinition->addTag('octave.menu.matcher_voter');
		$container->addDefinitions(['octave.test.voter' => $voterDefinition]);

		$pass->process($container);

		$this->assertTrue($itemMatcherDefinition->hasMethodCall('addVoter'));
		$this->assertEquals(count($itemMatcherDefinition->getMethodCalls()), 2);

		foreach ($itemMatcherDefinition->getMethodCalls() as $call)
		{
			$this->assertEquals($call[0], 'addVoter');
		}
	}

	/**
	 * Test if the pass works properly when the ItemMatcher service does not exist
	 */
	public function testProcessWithoutMatcher()
	{
		$pass = new VoterPass();
		$container = new ContainerBuilder();

		$pass->process($container);

		$this->assertFalse($container->hasDefinition(ItemMatcher::class));
	}
}