<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\Builder\BuilderInterface;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\BuilderPass;
use Octave\Bundle\MenuBundle\DependencyInjection\OctaveMenuExtension;
use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Integration tests for Octave\Bundle\MenuBundle\DependencyInjection\Compiler\BuilderPass
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class BuilderPassTest extends WebTestCase
{
	/**
	 * Test if adding builders to the MenuFactory service works
	 */
	public function testProcess()
	{
		$pass = new BuilderPass();
		$container = new ContainerBuilder();

		// Load services
		$extension = new OctaveMenuExtension();
		$extension->load([], $container);

		$menuFactoryDefinition = $container->getDefinition(MenuFactory::class);
		$pass->process($container);
		
		// Without a builder service the addBuilder method in the menu factory should not have been called
		$this->assertFalse($menuFactoryDefinition->hasMethodCall('addBuilder'));

		// Mock a builder with and without a key, the addBuilder method should now have been called twice in the menu factory
		$builderDefinitionWithKey = new Definition(BuilderInterface::class);
		$builderDefinitionWithKey->addTag('octave.menu.builder', [ 'key' => 'test-key' ]);
		$container->addDefinitions(['octave.test.builder_with_key' => $builderDefinitionWithKey]);

		$builderDefinitionWithoutKey = new Definition(BuilderInterface::class);
		$builderDefinitionWithoutKey->addTag('octave.menu.builder');
		$container->addDefinitions(['octave.test.builder_without_key' => $builderDefinitionWithoutKey]);

		$pass->process($container);

		$this->assertTrue($menuFactoryDefinition->hasMethodCall('addBuilder'));
		$this->assertEquals(count($menuFactoryDefinition->getMethodCalls()), 2);

		// Test if the keys with automatic fallback to service id works properly
		$this->assertEquals($menuFactoryDefinition->getMethodCalls()[0][1][1], 'test-key');
		$this->assertEquals($menuFactoryDefinition->getMethodCalls()[1][1][1], 'octave.test.builder_without_key');
	}

	/**
	 * Test if the pass works properly when the MenuFactory service does not exist
	 */
	public function testProcessWithoutFactory()
	{
		$pass = new BuilderPass();
		$container = new ContainerBuilder();

		$pass->process($container);

		$this->assertFalse($container->hasDefinition(MenuFactory::class));
	}
}