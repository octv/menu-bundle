<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\DependencyInjection\Compiler;

use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\ExtensionPass;
use Octave\Bundle\MenuBundle\DependencyInjection\OctaveMenuExtension;
use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Integration tests for Octave\Bundle\MenuBundle\DependencyInjection\Compiler\ExtensionPass
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class ExtensionPassTest extends WebTestCase
{
	/**
	 * Test if adding extensions to the MenuFactory service works
	 */
	public function testProcess()
	{
		$pass = new ExtensionPass();
		$container = new ContainerBuilder();

		// Load services
		$extension = new OctaveMenuExtension();
		$extension->load([], $container);

		$menuFactoryDefinition = $container->getDefinition(MenuFactory::class);
		$pass->process($container);
		
		// Without an extension service the addExtension method in the menu factory should not have been called
		$this->assertFalse($menuFactoryDefinition->hasMethodCall('addExtension'));

		// Mock an extension with and without a permission, the addExtension method should now have been called twice in the menu factory
		$extensionDefinitionWithPriority = new Definition(ExtensionInterface::class);
		$extensionDefinitionWithPriority->addTag('octave.menu.extension', [ 'builder' => 'test-builder', 'priority' => -10 ]);
		$container->addDefinitions(['octave.test.extension_with_priority' => $extensionDefinitionWithPriority]);

		$extensionDefinitionWithoutPriority = new Definition(ExtensionInterface::class);
		$extensionDefinitionWithoutPriority->addTag('octave.menu.extension', [ 'builder' => 'test-builder' ]);
		$container->addDefinitions(['octave.test.extension_without_priority' => $extensionDefinitionWithoutPriority]);

		$pass->process($container);

		$this->assertTrue($menuFactoryDefinition->hasMethodCall('addExtension'));
		$this->assertEquals(count($menuFactoryDefinition->getMethodCalls()), 2);

		// If the priority worked properly, the extension with negative priority that got added to the container first, should be added last
		$this->assertEquals((string)$menuFactoryDefinition->getMethodCalls()[0][1][0], 'octave.test.extension_without_priority');
		$this->assertEquals((string)$menuFactoryDefinition->getMethodCalls()[1][1][0], 'octave.test.extension_with_priority');
	}

	/**
	 * Test if the pass works properly when the MenuFactory service does not exist
	 */
	public function testProcessWithoutFactory()
	{
		$pass = new ExtensionPass();
		$container = new ContainerBuilder();

		$pass->process($container);

		$this->assertFalse($container->hasDefinition(MenuFactory::class));
	}
}