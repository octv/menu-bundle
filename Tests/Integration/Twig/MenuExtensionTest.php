<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Twig;

use Octave\Bundle\MenuBundle\Factory\MenuFactory;
use Octave\Bundle\MenuBundle\Renderer\TwigRenderer;
use Octave\Bundle\MenuBundle\Twig\MenuExtension;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Twig\MenuExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class MenuExtensionTest extends WebTestCase
{
	/**
	 * Test if rendering a menu works
	 */
	public function testRenderMenu()
	{
		$renderer = $this->getRenderer();
		$factory = $this->createMock(MenuFactory::class);
		$extension = new MenuExtension($factory, $renderer);

		$htmlWithoutTemplate = $extension->renderMenu('test_builder');
		$this->assertTrue(strlen($htmlWithoutTemplate) > 0);

		$htmlWithTemplate = $extension->renderMenu('test_builder', [], 'menu.html.twig');
		$this->assertTrue(strlen($htmlWithTemplate) > 0);
	}

	/**
	 * @return TwigRenderer
	 */
	private function getRenderer()
	{
		$loader = new FilesystemLoader(__DIR__.'/../../../Resources/views');
		$environment = new Environment($loader);
		$renderer = new TwigRenderer($environment);
		$renderer->setDefaultTemplate('menu.html.twig');

		return $renderer;
	}
}