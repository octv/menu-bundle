<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Tests\Integration\Builder;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Model\MenuInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Integration tests for Octave\Bundle\MenuBundle\Builder\AbstractBuilder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class AbstractBuilderTest extends WebTestCase
{
	/**
	 * Test if passing attributes to the builder works properly
	 */
	public function testMenuAttributes()
	{
		$builder = $this->getMockForAbstractClass(AbstractBuilder::class);
		$menu = $builder->build([
			'testAttributeKey' => 'testAttributeValue'
		]);

		$this->assertTrue($menu->hasAttribute('testAttributeKey'));
		$this->assertEquals($menu->getAttribute('testAttributeKey'), 'testAttributeValue');
	}

	/**
	 * Test if adding extensions to the builder works
	 */
	public function testExtensions()
	{
		$extension = $this->createMock(ExtensionInterface::class);
		$extension
			->expects($this->once())
			->method('extend')
			->with(
				$this->callback(function(MenuInterface $menu) {
					$menu->addChild('extensionChild');
					return true;
				})
			);
		;

		$builder = $this->getMockForAbstractClass(AbstractBuilder::class);
		$builder->addExtension($extension);

		$menu = $builder->build([]);

		$this->assertTrue($menu->hasChild('extensionChild'));
	}
}