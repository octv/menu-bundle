<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle;

use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\BuilderPass;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\ExtensionPass;
use Octave\Bundle\MenuBundle\DependencyInjection\Compiler\VoterPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OctaveMenuBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
		parent::build($container);

		$container->addCompilerPass(new BuilderPass());
		$container->addCompilerPass(new ExtensionPass());
		$container->addCompilerPass(new VoterPass());
	}
}