<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Matcher;

use Octave\Bundle\MenuBundle\Matcher\Voter\VoterInterface;
use Octave\Bundle\MenuBundle\Model\MenuItemCollectionInterface;
use Octave\Bundle\MenuBundle\Model\MenuItemInterface;

/**
 * Updates menu item active states based on voters
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class ItemMatcher
{
	/**
	 * @var VoterInterface[]
	 */
	protected $voters = [];

	/**
	 * Adds a voter to match the item against
	 *
	 * @param VoterInterface $voter
	 */
	public function addVoter(VoterInterface $voter)
	{
		$this->voters[] = $voter;
	}

	/**
	 * Matches the given menu item against existing voters and updates the active state if matched
	 * Returns true if the item matched against any voter
	 *
	 * @param  MenuItemInterface $item
	 *
	 * @return bool
	 */
	public function match(MenuItemInterface $item)
	{
		foreach ($this->voters as $voter)
		{
			if ($voter->vote($item)) {
				$item->setIsActive(true);
				return true;
			}
		}

		$item->setIsActive(false);
		return false;
	}

	/**
	 * Matches all items inside of a menu item collection
	 * Returns true if any of the children matched
	 *
	 * @param  MenuItemCollectionInterface $itemCollection
	 *
	 * @return bool
	 */
	public function matchCollection(MenuItemCollectionInterface $itemCollection)
	{
		$hasActiveChild = false;

		foreach ($itemCollection->getChildren() as $item)
		{
			if ($this->match($item)) {
				// This child is active
				$hasActiveChild = true;
			}

			if ($item->hasChildren() && $this->matchCollection($item)) {
				// A further nested child is active
				$hasActiveChild = true;
			}
		}

		if ($hasActiveChild && $itemCollection instanceof MenuItemInterface) {
			// One of its children is active, which means this item should be set to active aswell
			$itemCollection->setIsActive(true);
		}

		return $hasActiveChild;
	}
}