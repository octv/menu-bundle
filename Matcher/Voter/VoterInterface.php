<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Matcher\Voter;

use Octave\Bundle\MenuBundle\Model\MenuItemInterface;

/**
 * Interface for a menu matcher voter
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface VoterInterface
{
	/**
	 * Vote if the menu item is matched
	 * 
	 * @param  MenuItemInterface $item
	 * 
	 * @return bool
	 */
	public function vote(MenuItemInterface $item): bool;
}