<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Matcher\Voter;

use Octave\Bundle\MenuBundle\Model\MenuItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Matches a menu item if its' route is the current page
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class RouteVoter implements VoterInterface
{
	/**
	 * @var RequestStack
	 */
	protected $requestStack;

	/**
	 * RouteVoter constructor
	 *
	 * @param RequestStack $requestStack
	 */
	public function __construct(RequestStack $requestStack)
	{
		$this->requestStack = $requestStack;
	}

	/**
	 * Vote if the menu item is matched
	 * 
	 * @param  MenuItemInterface $item
	 * 
	 * @return bool
	 */
	public function vote(MenuItemInterface $item): bool
	{
		if (!$item->getRoute()) {
			return false;
		}

		$request = $this->requestStack->getCurrentRequest();

		if (null === $request) {
			return false;
		}

		// Check if the route matches
		$route = $request->attributes->get('_route');
		if (null === $route || $route != $item->getRoute()) {
			return false;
		}

		// Check if the route parameters match
		$routeParameters = $request->attributes->get('_route_params', []);
		foreach ($item->getRouteParameters() as $key => $parameter)
		{
			if (!isset($routeParameters[$key]) || $routeParameters[$key] != $parameter) {
				return false;
			}
		}

		return true;
	}
}