<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Builder;

use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Interface for a menu builder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface BuilderInterface
{
	/**
	 * Build the menu for the given attributes
	 * 
	 * @param  array $attributes
	 * 
	 * @return MenuInterface
	 */
	public function build(array $attributes): MenuInterface;

	/**
	 * Adds an extension to the menu builder
	 * 
	 * @param ExtensionInterface 	$extension
	 */
	public function addExtension(ExtensionInterface $extension);
}