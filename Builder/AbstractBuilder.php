<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Builder;

use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Abstract menu builder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
abstract class AbstractBuilder implements BuilderInterface
{
	/**
	 * @var ExtensionInterface[]
	 */
	protected $extensions = [];
	
	/**
	 * {@inheritdoc}
	 */
	public function build(array $attributes): MenuInterface
	{
		$menu = new Menu();
		$menu->setAttributes($attributes);

		$this->initialize($menu, $attributes);
		$this->loadExtensions($menu);

		return $menu;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addExtension(ExtensionInterface $extension)
	{
		$this->extensions[] = $extension;
	}

	/**
	 * Initializes the main items for the menu
	 * 
	 * @param MenuInterface 	$menu
	 * @param array 			$attributes
	 */
	abstract protected function initialize(MenuInterface $menu, array $attributes);

	/**
	 * Extend all extensions to the given menu
	 *
	 * @param MenuInterface $menu
	 */
	protected function loadExtensions(MenuInterface $menu)
	{
		foreach ($this->extensions as $extension)
		{
			$extension->extend($menu);
		}
	}
}