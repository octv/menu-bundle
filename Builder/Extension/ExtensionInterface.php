<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\MenuBundle\Builder\Extension;

use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Interface for a menu builder extension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
interface ExtensionInterface
{
	/**
	 * Extends the given menu
	 * 
	 * @param MenuInterface $menu
	 */
	public function extend(MenuInterface $menu);
}